/*
 * Copyright (C) 2014 Mukesh Y authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.imazze.fwginside;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CalElemets {
	//public static ArrayList<String> nameOfEvent = new ArrayList<String>();
	//public static ArrayList<String> startDates = new ArrayList<String>();
	//public static ArrayList<String> endDates = new ArrayList<String>();
	//public static ArrayList<String> descriptions = new ArrayList<String>();

	//public static ArrayList<String> readCalendarEvent(Context context) {
	//	return nameOfEvent;
	//}

	public static ArrayList<event> events = new ArrayList<event>();

	public static ArrayList<event> getEvents() {
		//ArrayList<event> events = new ArrayList<event>();
		//for(int n = 0; n< nameOfEvent.size(); n++){
		//	events.add(new event(nameOfEvent.get(n),startDates.get(n),endDates.get(n),descriptions.get(n)));
		//}
		return events;
	}


	//Füllt die Events in den Kalender!
	//public static void addElemet(String name, String startDate, String endDate, String descr){
		//nameOfEvent.add(name);
		//startDates.add(startDate);
		//endDates.add(endDate);
		//descriptions.add(descr);
	//}

	//Füllt die Events in deie einzelnen Arrays!
	public static void setElements(ArrayList<event> events){
		//for(event e:events){
		//	nameOfEvent.add(e.getTitle());
		//	startDates.add(e.getStartDate());
		//	endDates.add(e.getEndDate());
		//	descriptions.add(e.getDescription());
		//}
		CalElemets.events = events;

	}


	public static String getDate(long milliSeconds) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}


}
