package com.imazze.fwginside;

/**
 * Created by iMazze on 09.11.15.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "DB";

    // Contacts table name
    private static final String TABLE_CONTACTS = "timetable";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_ROOM = "room";
    private static final String KEY_TEACHER = "teacher";
    private static final String KEY_SUBJECT = "subject";
    private static final String KEY_COLOR = "color";
    private static final String KEY_DAY = "day";
    private static final String KEY_HOUR = "hour";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ROOM + " TEXT," + KEY_TEACHER + " TEXT," + KEY_SUBJECT + " TEXT," + KEY_COLOR + " TEXT," + KEY_DAY + " TEXT," + KEY_HOUR + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }



    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addLesson(lesson l) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ROOM, l.getRoom());
        values.put(KEY_TEACHER, l.getTeacher());
        values.put(KEY_SUBJECT, l.getSubject());
        values.put(KEY_COLOR, ""+l.getColor());
        values.put(KEY_DAY, ""+l.getDay());
        values.put(KEY_HOUR, ""+l.getHour());

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }

    public ArrayList<lesson> getAllLesons() {
        ArrayList<lesson> lessons = new ArrayList<lesson>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                lesson l = new lesson();
                l.setId(Integer.parseInt(cursor.getString(0)));
                l.setRoom(cursor.getString(1));
                l.setTeacher(cursor.getString(2));
                l.setSubject(cursor.getString(3));
                l.setColor(Integer.parseInt(cursor.getString(4)));
                l.setDay(Integer.parseInt(cursor.getString(5)));
                l.setHour(Integer.parseInt(cursor.getString(6)));

                // Adding contact to list
                lessons.add(l);
            } while (cursor.moveToNext());
        }

        return lessons;
    }

    public void updateLesson(lesson l) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ROOM, l.getRoom());
        values.put(KEY_TEACHER, l.getTeacher());
        values.put(KEY_SUBJECT, l.getSubject());
        values.put(KEY_COLOR, "" + l.getColor());
        values.put(KEY_DAY, "" + l.getDay());
        values.put(KEY_HOUR, "" + l.getHour());


        // updating row
        db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(l.getId())});
    }
}
