package com.imazze.fwginside;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class EditLessonActivity extends Activity {

	private EditText subject;
	private EditText teacher;
	private EditText room;
	private Spinner spinner;
	private int day;
	private int lesson;
	private boolean newEntry = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_lesson);

		day = getIntent().getIntExtra("day", 0);
		lesson = getIntent().getIntExtra("lesson", 0);

		if(day == 0 || lesson == 0) {
			finish();
		}



		subject = (EditText)findViewById(R.id.edit_subject);
		teacher = (EditText)findViewById(R.id.edit_teacher);
		room = (EditText)findViewById(R.id.edit_room);
		spinner = (Spinner)findViewById(R.id.spinner);

		//Color items
		List<String> list = new ArrayList<String>();
		list.add(""+Color.BLUE);
		list.add(""+Color.CYAN);
		list.add(""+Color.DKGRAY);
		list.add(""+Color.MAGENTA);
		list.add(""+Color.GREEN);
		list.add(""+Color.YELLOW);
		list.add(""+Color.RED);
		list.add(""+Color.TRANSPARENT);


		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);


		//Daten aus Datenbank auslesen
		for(lesson l:new SQLcon(this).getTimetable()){
			if(l.getDay()==day && l.getHour() == lesson){
				subject.setText(l.getSubject());
				teacher.setText(l.getTeacher());
				room.setText(l.getRoom());
				newEntry = false;
			}
		}

		Button submit = (Button)findViewById(R.id.submit_edit_lesson);
		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//TODO: Daten in die Datenbank einfügen
				int color = Integer.parseInt(String.valueOf(spinner.getSelectedItem()));
				if(newEntry){
					new SQLcon(getApplicationContext()).writeLesson(room.getText().toString(),teacher.getText().toString(),subject.getText().toString(), color,day,lesson);
				} else{
					new SQLcon(getApplicationContext()).updateLesson(room.getText().toString(), teacher.getText().toString(), subject.getText().toString(), color, day, lesson);
				}
				finish();
			}
		});
	}
}