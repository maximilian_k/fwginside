package com.imazze.fwginside;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by iMazze on 30.10.15.
 *
 * SIMULATION einiger SQL Abfragen
 */
public class SQLcon {


    private Context context;


    public SQLcon(Context context){
        this.context = context;
    }

    public boolean addEvent(String title, String startdate, String description, String klass){
        //Sql schicken!
        String result = "";
        InputStream isr = null;


        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://verwaltung.fwg-labor.de/fwginside/writeEvent.php"); //YOUR PHP SCRIPT ADDRESS

        try {
            // Add your data
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            //String date = sdf.format(new Date());

            List nameValuePairs = new ArrayList(2);
            nameValuePairs.add(new BasicNameValuePair("title", title));
            nameValuePairs.add(new BasicNameValuePair("startdate",startdate));
            nameValuePairs.add(new BasicNameValuePair("description",description));
            nameValuePairs.add(new BasicNameValuePair("class",klass));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;

            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }


            if(!new String(baf.toByteArray()).toLowerCase().startsWith("error")){//Kein Fehler
                //Weiterleitung zu home!
                //Intent i = new Intent(newMsgActivity.this, MainActivity.class);
                //finish();
                //startActivity(i);
                return true;
            } else {
                //Ausgabe
                Log.e("xxxF" , new String(baf.toByteArray()));
                return false;

            }

        } catch (ClientProtocolException e) {
            //
        } catch (IOException e) {
            // Auto-generated catch block
        }
        return false;

    }

    public ArrayList<event> getEvents(String user, String classes){
        ArrayList<event> events = new ArrayList<event>();

        //events.add(new event(99,"Dauerlauf2", "2015-10-30", "2015-10-30", "Dauerlauf VERPFLICHTEND für alle Klassen","sport"));
        //events.add(new event(99,"Schwimmen2", "2015-10-31", "2015-10-31", "Heute Schwimmen statt Turnen für Q11 (1spo)!","1spo"));

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String result = "";
        InputStream isr = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://verwaltung.fwg-labor.de/fwginside/getEvents.php"); //YOUR PHP SCRIPT ADDRESS
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            isr = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());

        }
        //convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(isr, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            isr.close();

            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error  converting result " + e.toString());
        }

        //parse json data
        try {
            JSONArray jArray = new JSONArray(result);

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json = jArray.getJSONObject(i);
                event ev = new event();
                //id,title,startdate,enddate,description,class
                ev.setId(json.getInt("id"));
                ev.setTitle(json.getString("title"));
                ev.setStartDate(json.getString("startdate"));
                ev.setEndDate(json.getString("enddate"));
                ev.setDescription(json.getString("description"));
                ev.setKlass(json.getString("class"));


                events.add(ev);
            }
        } catch (Exception e) {
            Log.e("log_tag", "Error Parsing Data " + e.toString());
        }





        //filtering
        ArrayList<event> filteredEvents = new ArrayList<event>();
        for (event e: events){
            if (containsClass(classes, e.getKlass())){
                filteredEvents.add(e);
            }
        }


        //Log.e("xxx1" , filteredEvents.toString());
        return filteredEvents;
    }

    public ArrayList<message> getMessages(String user, String classes) {
        ArrayList<message> msg = new ArrayList<message>();

        //    public message(int id, String author, String classes, String text, String date) {
        //msg.add(new message(99, "author", "[sport]", "text", "20150000_000000"));
        //msg.add(new message(99,"msmuell0","[mathe]","Mathematiktest heute in der 2. Stunde in G110","20150000_000000"));
        //msg.add(new message(99,"morsch","[all]","Schöne Ferien!","20150000_000000"));


        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String result = "";
        InputStream isr = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://verwaltung.fwg-labor.de/fwginside/getMsg.php"); //YOUR PHP SCRIPT ADDRESS
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            isr = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());

        }
        //convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(isr, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            isr.close();

            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error  converting result " + e.toString());
        }

        //parse json data
        try {
            JSONArray jArray = new JSONArray(result);

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json = jArray.getJSONObject(i);
                message m = new message();
                //    public message(int id, String author, String classes, String text, String date) {
                m.setId(json.getInt("id"));
                m.setAuthor(json.getString("author"));
                m.setClasses(json.getString("classes"));
                m.setText(json.getString("text"));
                m.setDate(json.getString("date"));


                msg.add(m);
            }
        } catch (Exception e) {
            Log.e("log_tag", "Error Parsing Data " + e.toString());
        }





        //filtering
        ArrayList<message> filteredEvents = new ArrayList<message>();
        for (message e: msg){
            if (containsClass2(classes, e.getClasses())){
                filteredEvents.add(e);
            }
        }


        //Log.e("xxx" , filteredEvents.toString());
        return filteredEvents;
    }

    public ArrayList<lesson> getTimetable(){


        DatabaseHandler db = new DatabaseHandler(context);


        // Reading all contacts
        ArrayList<lesson> content = db.getAllLesons();



        /*ArrayList<lesson> content = new ArrayList<lesson>();

        content.add(new lesson("G111", "Mn", "Mathe", Color.BLUE, 1, 1));
        content.add(new lesson("G111", "Mn", "Mathe", Color.BLUE, 1, 2));
        content.add(new lesson("G111", "Mn", "Mathe", Color.BLUE, 1, 6));
        content.add(new lesson("G111", "Mn", "Deutsch", Color.RED, 1, 5));
        content.add(new lesson("G111", "Mn", "Latein", Color.YELLOW, 2, 1));
        content.add(new lesson("G111", "Mn", "Latein", Color.YELLOW, 2, 2));
        content.add(new lesson("G111", "Mn", "Englisch", Color.GREEN, 2, 5));
        content.add(new lesson("G111", "Mn", "Mathe", Color.MAGENTA, 3, 2));
        content.add(new lesson("G111", "Mn", "Mathe", Color.LTGRAY, 4, 5));*/

        return content;
    }

    public void writeLesson(String room, String teacher, String subject, int color, int day, int hour){

        DatabaseHandler db = new DatabaseHandler(context);

        db.addLesson(new lesson(room, teacher, subject, color, day, hour));
    }

    public void updateLesson(String room, String teacher, String subject, int color, int day, int hour){

        DatabaseHandler db = new DatabaseHandler(context);

        db.updateLesson(new lesson(room, teacher, subject, color, day, hour));
    }

    private boolean containsClass(String classes, String key){
        //Log.e("xxx", "classes: " + classes + " --- key: " + key);

        if(classes.startsWith("[") && classes.endsWith("]")){
            //Array vorhanden
            classes = classes.replace("[","");
            classes = classes.replace("]","");

            String[] classArray = classes.split(",");

            for(int n=0;n<classArray.length;n++){
                //Log.e("xxx", classArray[n] + " --- " + key);
                if(classArray[n].equals(key)){
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }

    }

    private boolean containsClass2(String classes1, String classes2){
        //Log.e("xxx", "classes: " + classes + " --- key: " + key);

        if(classes1.startsWith("[") && classes1.endsWith("]") && classes2.startsWith("[") && classes2.endsWith("]")){
            //2 Arrays vorhanden
            classes1 = classes1.replace("[","");
            classes1 = classes1.replace("]","");
            classes2 = classes2.replace("[","");
            classes2 = classes2.replace("]","");

            String[] classArray1 = classes1.split(",");
            String[] classArray2 = classes2.split(",");


            for(int n=0;n<classArray1.length;n++){
                for(int m=0;m<classArray2.length;m++){
                    if(classArray1[n].equals( classArray2[m])){
                        //Log.e("xxx", classArray1[n] + " --- " + classArray1[m]);
                        return true;
                    }
                }

            }
            return false;
        } else {
            return false;
        }

    }
}
