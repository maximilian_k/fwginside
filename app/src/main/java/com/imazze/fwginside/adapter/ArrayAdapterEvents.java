package com.imazze.fwginside.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.imazze.fwginside.R;
import com.imazze.fwginside.event;

import java.util.List;


public class ArrayAdapterEvents extends ArrayAdapter<event>{

    private Context context;
    private List<event> events;


    public ArrayAdapterEvents(Context context, List<event> events) {
        super(context, R.layout.list_item_event, events);
        this.context = context;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.list_item_event, parent, false);

        TextView description = (TextView) row.findViewById(R.id.description_value);
        TextView klass = (TextView) row.findViewById(R.id.class_value);
        TextView title = (TextView) row.findViewById(R.id.title_value);


        klass.setText(events.get(position).getKlass());
        description.setText(events.get(position).getDescription());
        title.setText(events.get(position).getTitle());

        return row;
    }

    public void remove(int position){
        events.remove(position);
        notifyDataSetChanged();
    }


    public event getEvent(int position){
        return events.get(position);
    }
}