package com.imazze.fwginside.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.imazze.fwginside.R;
import com.imazze.fwginside.message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ArrayAdapterMsg extends ArrayAdapter<message>{

    private Context context;
    private List<message> messages;


    public ArrayAdapterMsg(Context context, List<message> messages) {
        super(context, R.layout.list_item_msg, messages);
        this.context = context;
        this.messages = messages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.list_item_msg, parent, false);




        //TextView id = (TextView) row.findViewById(R.id.id_value);
        TextView author = (TextView) row.findViewById(R.id.author_value);
        TextView date = (TextView) row.findViewById(R.id.date_value);
        TextView text = (TextView) row.findViewById(R.id.text_value);



        //id.setText(messages.get(position).getId()+"");
        author.setText(messages.get(position).getAuthor());
        //Datum setzen
        String dateStr="";
        dateStr = messages.get(position).getDate();
        //Sekunden weglassen
        dateStr = dateStr.substring(0,dateStr.length()-3);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String dateNow = sdf.format(new Date());
        if(dateStr.startsWith(dateNow)){
            dateStr = dateStr.replace(dateNow+" ","");
        } else {
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            try {
                Date date2 = sdf2.parse(dateStr);
                dateStr = date2.getDay() + ". " + getMonthString(date2.getMonth()) + " " + date2.getHours() + ":" + date2.getMinutes();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        date.setText(dateStr);

        text.setText(messages.get(position).getText());
        return row;
    }

    public String getMonthString(int month){
        String monthString;
        switch (month) {
            case 1:  monthString = "Jan.";       break;
            case 2:  monthString = "Feb.";      break;
            case 3:  monthString = "Mär.";         break;
            case 4:  monthString = "Apr.";         break;
            case 5:  monthString = "Mai";           break;
            case 6:  monthString = "Jun.";          break;
            case 7:  monthString = "Jul.";          break;
            case 8:  monthString = "Aug.";        break;
            case 9:  monthString = "Sep.";     break;
            case 10: monthString = "Okt.";       break;
            case 11: monthString = "Nov.";      break;
            case 12: monthString = "Dez.";      break;
            default: monthString = ""+month; break;
        }
        return monthString;
    }

    public void remove(int position){
        messages.remove(position);
        notifyDataSetChanged();
    }


    public message getMessage(int position){
        return messages.get(position);
    }
}