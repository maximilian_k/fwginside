package com.imazze.fwginside.adapter;

/**
 * Created by iMazze on 28.10.15.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.imazze.fwginside.fragments.FragmentKursleiter;
import com.imazze.fwginside.fragments.FragmentCal;
import com.imazze.fwginside.fragments.FragmentMsg;
import com.imazze.fwginside.fragments.FragmentTimetable;

import java.util.Locale;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new FragmentCal();
            case 1:
                return new FragmentMsg();
            case 2:
                return new FragmentTimetable();
            case 3:
                return new FragmentKursleiter();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return "Kalender";
            case 1:
                return "Nachrichten";
            case 2:
                return "Stundenplan";
            case 3:
                return "Kursleiter";
        }
        return null;
    }
}