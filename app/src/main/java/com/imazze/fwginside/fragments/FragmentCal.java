package com.imazze.fwginside.fragments;

/**
 * Created by iMazze on 29.10.15.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imazze.fwginside.CalElemets;
import com.imazze.fwginside.R;
import com.imazze.fwginside.SQLcon;
import com.imazze.fwginside.adapter.ArrayAdapterEvents;
import com.imazze.fwginside.adapter.CalendarAdapter;
import com.imazze.fwginside.event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class FragmentCal extends Fragment {


    public GregorianCalendar month, itemmonth;// calendar instances.

    public CalendarAdapter adapter;// adapter instance
    public Handler handler;// for grabbing some event values for showing the dot
    // marker.
    public ArrayList<String> items; // container to store calendar items which
    // needs showing the event marker
    //ArrayList<String> event;
    //ArrayList<String> date;
    //ArrayList<String> desc;

    ArrayList<event> events;

    ListView listItems;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.calendar, container, false);


        Locale.setDefault(Locale.GERMANY);

        month = (GregorianCalendar) GregorianCalendar.getInstance();
        itemmonth = (GregorianCalendar) month.clone();

        items = new ArrayList<String>();

        adapter = new CalendarAdapter(getActivity(), month);

        GridView gridview = (GridView) rootView.findViewById(R.id.gridview);
        gridview.setAdapter(adapter);

        handler = new Handler();
        handler.post(calendarUpdater);

        TextView title = (TextView) rootView.findViewById(R.id.title);
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        listItems = (ListView) rootView.findViewById(R.id.textList);

        RelativeLayout previous = (RelativeLayout) rootView.findViewById(R.id.previous);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        RelativeLayout next = (RelativeLayout) rootView.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                // removing the previous view if added
                listItems.setAdapter(new ArrayAdapterEvents(rootView.getContext(), new ArrayList<event>()));

                //desc = new ArrayList<String>();
                //date = new ArrayList<String>();
                events = new ArrayList<event>();
                ((CalendarAdapter) parent.getAdapter()).setSelected(v);
                String selectedGridDate = CalendarAdapter.dayString
                        .get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*",
                        "");// taking last part of date. ie; 2 from 2012-12-02.
                int gridvalue = Integer.parseInt(gridvalueString);
                // navigate to next or previous month on clicking offdays.
                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((CalendarAdapter) parent.getAdapter()).setSelected(v);

                for (int i = 0; i < CalElemets.events.size(); i++) {
                    if (CalElemets.events.get(i).getStartDate().equals(selectedGridDate)) {
                        //desc.add(CalElemets.nameOfEvent.get(i));
                        //events.add(new event(CalElemets.nameOfEvent.get(i), CalElemets.startDates.get(i), CalElemets.endDates.get(i), CalElemets.descriptions.get(i)));
                        events.add(CalElemets.events.get(i));
                    }
                }


                //Tagesevents (unten)
                if (events.size() > 0) {
                    ArrayList<event> eventList = new ArrayList<event>();
                    for (int i = 0; i < events.size(); i++) {
                        eventList.add(events.get(i));

                    }
                    listItems.setAdapter(new ArrayAdapterEvents(rootView.getContext(), eventList));

                }

                events = new ArrayList<event>();

            }

        });



        //Beispieldaten laden
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());

        String strUserName = SP.getString("username", null);
        String strClasses = SP.getString("classes" , "[]");
        CalElemets.setElements(new SQLcon(rootView.getContext()).getEvents(strUserName,strClasses));


        return rootView;
    }


    protected void setNextMonth() {
        if (month.get(GregorianCalendar.MONTH) == month.getActualMaximum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) + 1), month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) - 1), month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    protected void showToast(String string) {
        Toast.makeText(rootView.getContext(), string, Toast.LENGTH_SHORT).show();

    }

    public void refreshCalendar() {
        TextView title = (TextView) rootView.findViewById(R.id.title);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        handler.post(calendarUpdater); // generate some calendar items

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
    }

    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
            items.clear();

            // Print dates of the current week
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMANY);
            String itemvalue;
            events = CalElemets.getEvents();
            Log.d("=====Event====", events.toString());
            Log.d("=====Date ARRAY====", CalElemets.getEvents().toString());

            for (int i = 0; i < CalElemets.events.size(); i++) {
                itemvalue = df.format(itemmonth.getTime());
                itemmonth.add(GregorianCalendar.DATE, 1);
                items.add(CalElemets.events.get(i).getStartDate().toString());

            }
            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        }
    };

    public ArrayList<event> calcEvents(event e){

        //if(!CalElemets.events.get(i).getStartDate().equals(CalElemets.events.get(i).getEndDate())){
        //    for (event e: calcEvents(CalElemets.events.get(i))){
        //        events.add(e);
        //    }
       // }

        ArrayList<event> eventList = new ArrayList<event>();

        int n=0;
        Date dateStart=null;
        Date dateEnd=null;

        SimpleDateFormat sdf=null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateStart = sdf.parse(e.getStartDate());
            dateEnd = sdf.parse(e.getEndDate());

            n=dateEnd.getDay()-dateStart.getDay();

        } catch (ParseException e1) {
            e1.printStackTrace();
        }

        for (int m=0;m<n;m++){
            event tmpevent = e;

            Log.e("xxx" , m+"");
            Calendar c = Calendar.getInstance();
            c.setTime(dateStart);
            c.add(Calendar.DATE, m);  // number of days to add

            tmpevent.setStartDate(sdf.format(c.getTime()));
            eventList.add(tmpevent);
        }

        return eventList;
    }
}