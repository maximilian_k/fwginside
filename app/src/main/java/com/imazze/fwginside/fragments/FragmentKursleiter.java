package com.imazze.fwginside.fragments;

/**
 * Created by iMazze on 28.10.15.
 *
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.imazze.fwginside.R;
import com.imazze.fwginside.SQLcon;

public class FragmentKursleiter extends Fragment {

    View rootView;

    String m_Text = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_kursleiter, container, false);

        Button create = (Button) rootView.findViewById(R.id.btnCreate);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // create a Dialog component
                final Dialog dialog = new Dialog(rootView.getContext());

                //tell the Dialog to use the dialog.xml as it's layout description
                dialog.setContentView(R.layout.dialog_newevent);
                dialog.setTitle("Neuer Kalendereintrag");

                //TextView txt = (TextView) dialog.findViewById(R.id.txt);
                final EditText title = (EditText) dialog.findViewById(R.id.entryTfTitle);
                final EditText date = (EditText) dialog.findViewById(R.id.entryTfDate);
                final EditText description = (EditText) dialog.findViewById(R.id.entryTfDescr);
                final EditText classes = (EditText) dialog.findViewById(R.id.entryTfClasses);


                Button dialogButton = (Button) dialog.findViewById(R.id.dialogBtnSend);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        //Action

                        if (new SQLcon(rootView.getContext()).addEvent(title.getText().toString(),date.getText().toString(),description.getText().toString(),classes.getText().toString())){
                            //Fehlerfrei
                            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(rootView.getContext());
                            dlgAlert.setMessage("Kalendereintrag '" + title.getText() + "' erfolgreich erstellt!");
                            dlgAlert.setTitle("FWGinside");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.setCancelable(false);
                            dlgAlert.create().show();
                        }



                    }
                });

                dialog.show();
            }
        });
        return rootView;
    }
}