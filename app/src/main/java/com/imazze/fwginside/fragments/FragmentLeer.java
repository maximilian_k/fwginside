package com.imazze.fwginside.fragments;

/**
 * Created by iMazze on 28.10.15.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imazze.fwginside.R;

public class FragmentLeer extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_leer, container, false);

        return rootView;
    }

}