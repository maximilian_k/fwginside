package com.imazze.fwginside.fragments;

/**
 * Created by iMazze on 28.10.15.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.imazze.fwginside.R;
import com.imazze.fwginside.SQLcon;
import com.imazze.fwginside.adapter.ArrayAdapterMsg;
import com.imazze.fwginside.message;

import java.util.ArrayList;
import java.util.List;

public class FragmentMsg extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ArrayAdapterMsg adapter;

    private ListView listV;
    public List<message> messages;
    SwipeRefreshLayout swipeRefreshLayout;

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_msg, container, false);


        listV = (ListView) rootView.findViewById(R.id.listMsg);

        //Liste der Messages
        adapter = new ArrayAdapterMsg(rootView.getContext(),  new ArrayList<message>()); //Vertretungen am Anfang LEER
        listV.setAdapter(adapter);
        // Click event for single list row
        listV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //showDetail(position);
            }
        });






        //Swipe Refresh
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());

                String strUserName = SP.getString("username", null);
                String strClasses = SP.getString("classes" , "[]");
                getData(strUserName, strClasses);
            }
        });



        return rootView;
    }

    @Override
    public void onRefresh() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());

        String strUserName = SP.getString("username", null);
        String strClasses = SP.getString("classes" , "[]");
        getData(strUserName,strClasses);
    }

    public void getData(String user, String classes){


        listV.setAdapter(new ArrayAdapterMsg(rootView.getContext(),new SQLcon(rootView.getContext()).getMessages(user,classes)));
        swipeRefreshLayout.setRefreshing(false);


    }
}