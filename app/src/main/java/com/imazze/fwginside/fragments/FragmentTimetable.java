package com.imazze.fwginside.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.imazze.fwginside.EditLessonActivity;
import com.imazze.fwginside.R;
import com.imazze.fwginside.SQLcon;
import com.imazze.fwginside.lesson;

import java.util.ArrayList;

/**
 * Created by iMazze on 09.11.15.
 */
public class FragmentTimetable extends Fragment {//extends OptionsMenuEnabledActvity {

    View rootView;
    public static final int EDIT = 0b1;
    public static final int VIEW = 0b10;

    private int mode;

    private ArrayList<lesson> lessons;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_timetable, container, false);

        GridView timetable = (GridView) rootView.findViewById(R.id.timetable_grid);
        timetable.setAdapter(new TimetableAdapter());

        //mode = rootView.getIntent().getIntExtra("mode", VIEW);

        //if(mode == EDIT) {
        //    registerForContextMenu(timetable);
        //}

        /*timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = (int)view.getX() % (getResources().getInteger(R.integer.num_days)+1);
                int y = (int)view.getY() / (getResources().getInteger(R.integer.num_days)+1);

                Log.e("xxx","x:"+x+", y:"+y);
            }
        });*/


        timetable.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int x = i % (getResources().getInteger(R.integer.num_days)+1);
                int y = i / (getResources().getInteger(R.integer.num_days)+1);

                Intent intent = new Intent(rootView.getContext(), EditLessonActivity.class);
                intent.putExtra("day", x);
                intent.putExtra("lesson", y);
                startActivity(intent);

            }
        });

        lessons = new SQLcon(rootView.getContext()).getTimetable();






        return rootView;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        lessons = new SQLcon(rootView.getContext()).getTimetable();
    }

    private class TimetableAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return (getResources().getInteger(R.integer.num_days)+1)*(getResources().getInteger(R.integer.num_lessons)+1);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Resources res = getResources();
            TextView view;
            if(convertView == null) {
                view = new TextView(rootView.getContext());//FragmentTimetable.TimetableAdapter.this);
                view.setTextColor(Color.BLACK);
                view.setBackgroundColor(Color.WHITE);
            }else {
                view = (TextView)convertView;
            }

            int x = position % (res.getInteger(R.integer.num_days)+1);
            int y = position / (res.getInteger(R.integer.num_days)+1);

            if(position == 0) {
                view.setText("Stunde");
            }else if(position < res.getInteger(R.integer.num_days) + 1) {
                view.setText(res.getStringArray(R.array.days_short)[position-1]);
            }else if(x == 0) {
                view.setText(y+"\n"+
                        res.getStringArray(R.array.lesson_start_times)[y-1]+"\n-"+
                        res.getStringArray(R.array.lesson_end_times)[y-1]);
            }else {
                //x=Tag (1=Montag)
                //y=Stunde (1=1. Stunde)

                view.setText("\n/\n");
                view.setBackgroundColor(Color.WHITE);

                for (lesson l:lessons){
                    if(l.getDay() == x && l.getHour() == y){
                        view.setText(l.getSubject() + "\n" + l.getTeacher() + "\n" + l.getRoom());
                        view.setBackgroundColor(l.getColor());
                    }
                }
            }
            return view;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    }
}
