package com.imazze.fwginside;

public class lesson {

	private int id;
	private String subject;
	private String teacher;
	private String room;
	private int color;
	private int day;
	private int hour;

	public lesson(String room, String teacher, String subject, int color, int day, int hour) {
		this.subject = subject;
		this.teacher = teacher;
		this.room = room;
		this.color = color;
		this.day = day;
		this.hour = hour;
	}

	public lesson(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
}