package com.imazze.fwginside;

/**
 * Created by iMazze on 07.10.15.
 */
public class message {
    private int id;
    private String author;
    private String classes;
    private String text;
    private String date;

    public message(int id, String author, String classes, String text, String date) {
        this.id = id;
        this.author = author;
        this.classes = classes;
        this.text = text;
        this.date = date;
    }

    public message() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        String tmpdate = date.substring(6,8) + "." + date.substring(4,6) + "." + date.substring(0,4) + " " +date.substring(9,11) + ":" + date.substring(11,13) + ":" + date.substring(13,15);
        return tmpdate;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
